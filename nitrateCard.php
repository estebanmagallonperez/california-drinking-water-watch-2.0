<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Home Page for UC Water">
        <meta name="author" content="Esteban Perez">

        <title>UC Water</title>
        <!-- <link href="./css/bootstrap.min.css" rel="stylesheet">-->
        <!--<link href="./css/water.css" rel="stylesheet">-->

        <style>
            body {
                font-family: Sans-Serif;
            }
            .card
            {
                padding: 10px;
                /*    background: white;
                border-radius: 5px;
                box-shadow: 0px 0px 5px #888888;
                padding: 10px;
                margin-bottom: 15px;
                padding: 2% 5% 10% 5%;*/
            }
            .card .head
            {
                text-align: center;
                width: 100%;

            }
            .card .body
            {

            }
            .body h2
            {
                text-align:center;
            }
            .outerBar
            {
                width: 100%;
                border:solid 1px black;
                height: 20px;
                border-radius: 5px;
                overflow: hidden;
                background: linear-gradient(to right, #abc56c,#abc56c,#abc56c,#abc56c,#ffca00,#ffca00,#cd665f,#cd665f);
            }
            .innerBar
            {
                position: absolute;
                right: 0px;
                transition: all 1s ease;
                height: 100%;
                background: white;
                border-left: solid 1px white;

            }
            #tooltip
            {
                position:absolute;
                background:white;
                border-radius:5px;
                box-shadow: 0px 0px 5px #888888;
                bottom:110%;
                width:90%;
                padding:5%;/*
                transform:translate(-50%,0px);
                left:50%*/
            }
            #contentLoad:hover #tooltip{
                display: block!important;
            }

            .hidden
            {
                display: none;
            }
        </style>

    </head>
    <body>

        <script src="js/libraries/tether.min.js"></script>
        <script src="./js/libraries/jquery-3.1.1.min.js"></script>
        <!--<script src="js/custom/map.js."></script>-->
        <script src="./js/libraries/bootstrap.min.js"></script>


        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <div class="card">
            <div class="head">
                <h1>Nitrate</h1>
            </div>
            <div class="body">
                <div class="hidden">
                    <p style="text-align:center" id="violations">There have been no violations in your distict</p>
                    <!--<h2>2017</h2>-->

                    <hr>
                    <p> Nitrates have been linked to:</p>
                    <ul>
                        <li>Blue Baby Syndrome</li>
                    </ul>
                    <p>Others who are at risk are </p>
                    <ul>
                        <li>Pregnant women</li>
                        <li>Individuals with reduced gastric acidity</li>
                        <li>Individuals with a hereditary lack of methemoglobin reductase</li>
                    </ul>
                    <div id="contentLoad" style="position:relative;">
                        <div id="tooltip" style="display:none">
                            Your water supply has an avereage nitrate level of: <span id="nitrate">adsf </span>mg/l<br>
                            This water is <span id="context"></span>
                        </div>
                        <p>The most recent test for nitrate was in <span id="year">year</span></p>
                        <div style="position:relative">
                            <div style="position:relative" class="outerBar">
                                <div id="inner"class="innerBar"></div>
                                <div style="position:absolute;left:80%;top:0px;border:solid 1px black;height:100%;"></div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="visible">
                    Please Wait while we load this data.
                </div>
            </div>


        </div>




        <?php
        $tinwsys = $_GET["tinwsys"];
        echo "<script>var tinwsys = ".$tinwsys.";var pwsid=".$_GET["pwsid"]."</script>";
        ?>
        <script>
            function setQuality(results)
            {
                console.log("result is: "+results[2]);
                var test = results[2];
                var overlay;
                var context;
                console.log(test<=10);
                /*      0-10 Safe for humans and livestock. However, concentrations of more than 4 ppm are an
            indicator of possible pollution sources and could cause environmental problems.
            11-20 Generally safe for human adults and livestock. Not safe for infants because their
            digestive systems cannot absorb and excrete nitrate.
            21-40 Should not be used as a drinking water source but short-term use acceptable for
                adults and all livestock unless food or feed sources are very high in nitrates.
                41-100 Risky for adults and young livestock. Probably acceptable for mature livestock if
                    feed is low in nitrates.
                    Over 100 Should not be used as drinking water for humans or livestock. */
                if(test<=10){
                    context = "safe for humans and livestock.";
                    console.log(test);
                    overlay=80;
                }else if(test<=20){
                    console.log(test);
                    context = "generally safe for human adults and livestock, but not safe for infants.";
                    overlay=50;
                }else if(test<=40){
                    console.log(test);
                    context = "not to be used as a drinking water source, but short-term use is acceptable for adults.";
                    overlay=40;
                }else if(test<=100){
                    context = "risky for adults and young livestock."
                    console.log(test);
                    overlay=30;
                }else{
                    context = "should not be used as drinking water for humans or livestock."
                    console.log(test);
                    overlay=20;
                }

                console.log(overlay);
                if(results[3] != 0){
                    document.getElementById("inner").style.width=overlay+"%";
                    document.getElementById("year").innerHTML=results[3];
                    document.getElementById("nitrate").innerHTML = Math.round(test*100)/100;
                    document.getElementById("context").innerHTML = context;
                }else
                {
                    $("#contentLoad").html("There are no measurements of nitrate in this district.");
                }
                //document.getElementById("")
            }
            if(tinwsys && pwsid){


                console.log("we are doing it");
                var urlDest = "./php/getNitrates.php?tinwsys="+tinwsys+"&pwsid="+pwsid;
                var jqxhr = $.ajax( {url: urlDest,
                                     timeout:60000 })
                .done(function() {
                    var out = jqxhr.responseText.split(',');
                    console.log('done');
                    console.log(out);
                    if(out.indexOf("error") >=0)
                    {
                    }else
                    {
                        getViolation();
                        $(".hidden").toggleClass("hidden");
                        $(".visible").remove()
                        var result = jqxhr.responseText;
                        setQuality(out);
                    }
                })
                .fail(function() {
                    console.log("failed");
                })
                }
            function getViolation()
            {
                $.getJSON("./js/violations.json", function(json) {
                    //console.log(json);
                    for(var i = 0; i < json.length;i++)
                    {
                        if(json[i].tinSys == tinwsys)
                        {
                            console.log(json[i])
                            for(var j =0;i<json[i].violations.length;j++)
                            {
                                console.log(json[i].violations[j].violationName);  if(json[i].violations[j].violationName.includes("NITRATE"))
                                {
                                    document.getElementById("violations").innerHTML = "There have been "+json[i].violations[j].years.length+" violations for Nitrate, the most recent violation was in "+json[i].violations[j].years[0];
                                    length++;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                })
            }

        </script>


    </body>
</html>
