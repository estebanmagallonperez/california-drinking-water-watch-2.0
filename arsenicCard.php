<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Home Page for UC Water">
        <meta name="author" content="Esteban Perez">

        <title>UC Water</title>
        <!-- <link href="./css/bootstrap.min.css" rel="stylesheet">-->
        <!--<link href="./css/water.css" rel="stylesheet">-->

        <style>
            body {
                font-family: Sans-Serif;
            }
            .card
            {
                padding: 10px;
                /*    background: white;
                border-radius: 5px;
                box-shadow: 0px 0px 5px #888888;
                padding: 10px;
                margin-bottom: 15px;
                padding: 2% 5% 10% 5%;*/
            }
            .card .head
            {
                text-align: center;
                width: 100%;

            }
            .card .body
            {

            }
            .body h2
            {
                text-align:center;
            }
            .outerBar
            {
                width: 100%;
                border:solid 1px black;
                height: 20px;
                border-radius: 5px;
                overflow: hidden;
                background: linear-gradient(to right, #abc56c,#abc56c,#abc56c,#abc56c,#ffca00,#ffca00,#cd665f,#cd665f);
            }
            .innerBar
            {
                position: absolute;
                right: 0px;
                transition: all 1s ease;
                height: 100%;
                width: 50%;
                background: white;
                border-left: solid 1px white;

            }
            .hidden
            {
                display: none;
            }
            #tooltip
            {
                position:absolute;
                background:white;
                border-radius:5px;
                box-shadow: 0px 0px 5px #888888;
                bottom:110%;
                width:90%;
                padding:5%;/*
                transform:translate(-50%,0px);
                left:50%*/
            }
            #contentLoad:hover #tooltip{
                display: block!important;
            }
        </style>

    </head>
    <body>

        <script src="js/libraries/tether.min.js"></script>
        <script src="./js/libraries/jquery-3.1.1.min.js"></script>
        <script src="./js/libraries/bootstrap.min.js"></script>


        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <div class="card">
            <div class="head">
                <h1>Arsenic</h1>
            </div>
            <div class="body">

                <div class="hidden">
                    <p style="text-align:center" id="violations">There have been no violations in your distict</p>
                    <hr>
                    <p> Short term effects of Arsenic include:</p>
                    <ul>
                        <li>Nausea</li>
                        <li>Vomiting</li>
                        <li>Numbness or burning sensations in the hands and feet</li>
                        <li>Fatigue</li>
                    </ul>
                    <p> Long term effects of Arsenic include:</p>
                    <ul>
                        <li>Changes in skin coloration</li>
                        <li>Skin thickening</li>
                    </ul>
                    <p> Chronic exposure to arsenic is also associated with an increased risk of skin, bladder, and lung cancer.</p>
                    <div id="contentLoad" style="position:relative;">
                        <div id="tooltip" style="display:none">
                            Your water supply has an avereage arsenic level of: <span id="arsenic">adsf </span>UG/L<br>
                            This water has<span id="context"></span>
                        </div>
                        <p>The most recent test for arsenic was in <span id="year">year</span></p>
                        <div style="position:relative">
                            <div style="position:relative" class="outerBar">
                                <div id="inner"class="innerBar"></div>
                                <!--<div style="position:absolute;left:60%;top:0px;border:solid 1px black;height:100%;"></div>-->

                            </div>
                        </div>
                    </div>
                </div>
                <div class="visible">
                    Please Wait while we load this data.
                </div>


            </div>


        </div>



        <?php
        $tinwsys = $_GET["tinwsys"];
        echo "<script>var tinwsys = ".$tinwsys.";var pwsid=".$_GET["pwsid"]."</script>";
        ?>
        <script>

            function setQuality(results)
            {
                var test = results[2];
                var overlay;
                var context = "";
                if(test > 10)
                {
                    overlay = 15;
                    context = " toxic levels of arsenic.";
                }else
                {
                    overlay = 80;
                    context = " safe levels of arsenic.";
                }
                if(results[3] != 0){
                    document.getElementById("inner").style.width=overlay+"%";
                    document.getElementById("year").innerHTML=results[3];
                    document.getElementById("arsenic").innerHTML = Math.round(test*100)/100;
                    document.getElementById("context").innerHTML = context;
                }else
                {
                    $("#contentLoad").html("There are no measurements of arsenic in this district.");
                }
            }
            if(tinwsys && pwsid){
                var urlDest = "./php/getArsenic.php?tinwsys="+tinwsys+"&pwsid="+pwsid;
                var jqxhr = $.ajax( {url: urlDest,
                                     timeout:60000 })
                .done(function() {
                    var out = jqxhr.responseText.split(',');
                    if(out.indexOf("error") >=0)
                    {
                    }else
                    {
                        getViolation();
                        $(".hidden").toggleClass("hidden");
                        $(".visible").remove();
                        var result = jqxhr.responseText;
                        setQuality(out);
                    }
                })
                .fail(function() {
                    console.log("failed");
                })
                }
            function getViolation()
            {
                $.getJSON("./js/violations.json", function(json) {
                    for(var i = 0; i < json.length;i++)
                    {
                        if(json[i].tinSys == tinwsys)
                        {
                            for(var j =0;j<json[i].violations.length;j++)
                            {
                                if(json[i].violations[j].violationName.includes("ARSENIC"))
                                {
                                    document.getElementById("violations").innerHTML = "There have been "+json[i].violations[j].years.length+" violations for Arsenic, the most recent violation was in "+json[i].violations[j].years[0];
                                    length++;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                })
            }
        </script>

    </body>
</html>
