<?php

//Hours Spent 12 + 5
$tinwsys = $_GET["tinwsys"];
include('simple_html_dom.php');

$monitoringUrl = "https://sdwis.waterboards.ca.gov/PDWW/JSP/Violations.jsp?tinwsys_is_number=".$tinwsys."&tinwsys_st_code=CA";
$html = file_get_html( $monitoringUrl);
//echo $html;
$violations=array();
$temp = 0;
foreach($html->find('tr') as $e){
	if($e->children(4)){
		$splitLoc = strpos($e->children(4)->children(0)->innertext, "Analyte");
		// echo "split".$splitLoc."<br>";

		if ($splitLoc != "39") {
			//echo $e->children(4)->children(0)->innertext."<br>";
			$add = 1;
			for($i = 0;$i<count($violations);$i+=3)
			{
				if($violations[$i] == $e->children(4)->children(0)->innertext)
				{
					$add = 0;
					$dateString = $e->children(0)->children(0)->children(0)->innertext;
					$dateString = substr($dateString,0, strpos($dateString, '-'));
					$violations[$i+2] = $violations[$i+2].",".$dateString;
					break;
				}
			}
			if($add){
				array_push($violations,strip_tags(preg_replace('/\s+/', ' ',$e->children(4)->children(0)->innertext)));
				//add violation name
				array_push($violations,strip_tags(preg_replace('/\s+/', ' ',$e->children(5)->children(0)->innertext)));
				//add violation year
				$dateString = $e->children(0)->children(0)->children(0)->innertext;
				$dateString =preg_replace('/\s+/', ' ', substr($dateString,0, strpos($dateString, '-')));
				array_push($violations,$dateString);
			}

		}
	}
}
//[{violationNum:"0200",violationName:"SWTR ",years:[12,12,13,14],violationCount:"2"}]
echo "[";
for($i = 0;$i<count($violations);$i+=3)
{
	echo "{";
	echo '"violationNum":"'.$violations[$i].'",';
	echo '"violationName":"'.$violations[$i+1].'",';
	echo '"years":['.$violations[$i+2].']';
	echo "}";
	if($i+4<count($violations)){echo ",";}
}
echo "]";
?>
