<?php
$tinwsys = $_GET["tinwsys"];
$pwsid = $_GET['pwsid'];
include('simple_html_dom.php');
$monitoringUrl = "https://sdwis.waterboards.ca.gov/PDWW/JSP/SamplingPointsTable.jsp?tinwsys_is_number=".$tinwsys."&tinwsys_st_code=CA";
$html = file_get_html( $monitoringUrl);
$treatedFacilities=array();
$temp = 0;
$vals =0;
$counts =0;
foreach($html->find('tr') as $e){
	if($e->children(0)->children(0)->children(0) && $temp < 4){
		$splitLoc = strpos($e->children(0)->children(0)->children(0)->innertext, '-');
		if ($splitLoc != "-1") {
			//echo "<br>".substr($e->children(0)->children(0)->children(0)->innertext,$splitLoc+1	)."<br>";
			array_push($treatedFacilities,substr($e->children(0)->children(0)->children(0)->innertext,$splitLoc+1	));
			$temp++;
		}
	}
}
$today = getdate();
$day = "";
$month = "";
$year = $today['year'];
if($today['mon'] < 10){
	$month = "0".$today['mon'];
}else{
	$month = $today['mon'];
}
if($today['mday'] < 10){
	$day = "0".$today['mday'];
}else{
	$day = $today['mday'];
}
$recentYear = 0;
foreach($treatedFacilities as $samplingPoint)
{
	/*get the table with results from previous monitorings over the past year. for each treated faculity that we found in the last search*/
	$detailedReport = "https://sdwis.waterboards.ca.gov/PDWW/JSP/SamplingResultsByStoret.jsp?SystemNumber=".$pwsid."&SamplingPointName=&SamplingPointID=".$samplingPoint."&Storet=71850&ChemicalName=";
	$detailedReport = $detailedReport."&begin_date=".$month."%2F".$day."%2F".($year - 10);
	$detailedReport = $detailedReport."&end_date=".$month."%2F".$day."%2F".$year;
	$detailedReport = $detailedReport."&Generate+Report=Generate+Report";

	//echo "<br><br>".$detailedReport."<br><br>";
	$html = file_get_html($detailedReport);
	//echo $html;
	/*Get the values, found at each sampling point.*/
	foreach($html->find('#3 tbody tr') as $e){
		if (strpos($e->children(2)->innertext, 'NITRATE') !== false) {
			//4.42 converts from nitrate in form N to nitrate in form no3
			$counts++;
			$vals+=($e->children(6)->innertext);
			$pos = strpos($e->children(4)->innertext, '-');
			$temp = substr($e->children(4)->innertext,0,$pos);
			$tempNum= (int)$temp;
			if($recentYear < $tempNum)
			{
				$recentYear = $tempNum;
			}
		}
	}
}
/*video conference 9*/
echo $counts.",".$vals.",";
if($counts != 0){
	echo ($vals/$counts);
}else
{
	echo "-1";
}
echo ",".$recentYear;
?>
