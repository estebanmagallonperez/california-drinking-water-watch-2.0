<?php
/*Library used for parsing through data*/
include('simple_html_dom.php');
/*variable declaration*/

/*used for setting up urls for data retrieval*/
$tinwsys = $_GET["tinwsys"];
$pwsid = $_GET['pwsid'];
/*initiallizing vals, which stores the average value found for each parameter, count which stores the number found, for averaging, weights which are the respecive weights of all the parameters, adjustedWeight which is used to adjust the weights when we are missing parameters. and functions which are used as a goodness measure for each parameter.*/
$vals =array(0,0,0,0,0,0,0,0,0);
$counts =array(0,0,0,0,0,0,0,0,0);
$weights = array(0.17,0.16,0.11,0.11,0.10,0.10,0.10,0.08,0.07);
$adjustedWeight = 0;
$functionNames = array('disolvedOxygen','FecalColiform','PH','BOD','tempChange','phosphate','nitrates','turbidity','totalSolids');
$score = 0;


/*an array of function which model the goodness rating provided from the graphs at http://www.water-research.net/*/
$functions = array(
	'disolvedOxygen' => function($Val) {
	return (1.212121 - 0.3479798*$Val + 0.0417803*$Val^2 - 0.0003724747*$Val^3 + 8.522727e-7*$Val^4);
},
	'FecalColiform' => function($Val) {
	return (1.114311 + (100.3168 - 1.114311)/(1 + ($Val/60.12186)^0.47579));
},
	'PH' => function($Val) {
	//Returning proper val --tested
	return 90.16164*pow(M_E,-pow(-(8.3 - 7.412419),2)/4.33285558128);
},
	'BOD' => function($Val) {
	return (99.49688 - (11.26095/0.1157408)*(1 - M_E^(-0.1157408*$Val)));
},
	'tempChange' => function($Val) {
	return (82.48728*M_E^(-($Val - -0.4976515)^2/(2*11.12208^2)) );
},
	'phosphate' => function($Val) {
	//Returning proper val --tested
	return (98.18296 - (89.427411964794)*(1 - pow(M_E,(-0.8585003*$Val))));
},
	'nitrates' => function($Val) {
	//Returning proper val --tested
	return (96.5569 - (93.39654973218)*(1 - pow(M_E,(-0.05150035*$Val/4.42) )));
},
	'turbidity' => function($Val) {
	//Returning proper val --tested
	return (98.25774 - (2.17882/0.02553104)*(1 - pow(M_E,(-0.02553104*$Val) )));
},
	'totalSolids' => function($Val) {
	//Returning proper val --tested
	return (80.13986 + 0.2043823*$Val - 0.002302098*pow($Val,2) + 0.000006573427*pow($Val,3) - 6.526807e-9*pow($Val,4));
}
);

/*retrieve all treated water sources from waterboards webstie*/
$monitoringUrl = "https://sdwis.waterboards.ca.gov/PDWW/JSP/SamplingPointsTable.jsp?tinwsys_is_number=".$tinwsys."&tinwsys_st_code=CA";
$html = file_get_html( $monitoringUrl);
$treatedFacilities=array();
$temp = 0;

foreach($html->find('tr') as $e){
	if($e->children(0)->children(0)->children(0) && $temp < 4){
		$splitLoc = strpos($e->children(0)->children(0)->children(0)->innertext, '-');
		if ($splitLoc != "-1") {
			//echo "<br>".substr($e->children(0)->children(0)->children(0)->innertext,$splitLoc+1	)."<br>";
			array_push($treatedFacilities,substr($e->children(0)->children(0)->children(0)->innertext,$splitLoc+1	));
			$temp++;
		}
	}
}
/*format the date so we can pull the results from the past year*/
$today = getdate();
$day = "";
$month = "";
$year = $today['year'];
if($today['mon'] < 10){
	$month = "0".$today['mon'];
}else{
	$month = $today['mon'];
}
if($today['mday'] < 10){
	$day = "0".$today['mday'];
}else{
	$day = $today['mday'];
}

foreach($treatedFacilities as $samplingPoint)
{
	/*get the table with results from previous monitorings over the past year. for each treated faculity that we found in the last search*/
	$detailedReport = "https://sdwis.waterboards.ca.gov/PDWW/JSP/SamplingResultsByStoret.jsp?SystemNumber=".$pwsid."&SamplingPointName=&SamplingPointID=".$samplingPoint."&Storet=&ChemicalName=";
	$detailedReport = $detailedReport."&begin_date=".$month."%2F".$day."%2F".($year - 1);
	$detailedReport = $detailedReport."&end_date=".$month."%2F".$day."%2F".$year;
	$detailedReport = $detailedReport."&Generate+Report=Generate+Report";

	//echo "<br><br>".$detailedReport."<br><br>";
	$html = file_get_html($detailedReport);

	/*Get the values, found at each sampling point.*/
	foreach($html->find('#3 tbody tr') as $e){
		//if(check for disollved oxygen){$vals[0]+=result;}
		//if(check for fecal coliform){$vals[1]+=result;}
		if (strpos($e->children(2)->innertext, 'PH,') !== false) {
			$counts[2]++;
			$vals[2]+=$e->children(6)->innertext;
		}
		//if(check for BOD){vals[3]+=result;}
		//if(check for TemperatureChange){vals[4]+=result;}
		if (strpos($e->children(2)->innertext, 'PHOSPHATE,') !== false) {
			$counts[5]++;
			$vals[5]+=$e->children(6)->innertext;
		}
		if (strpos($e->children(2)->innertext, 'NITRATE') !== false) {
			//4.42 converts from nitrate in form N to nitrate in form no3
			$counts[6]++;
			$vals[6]+=($e->children(6)->innertext*4.42);
		}
		if (strpos($e->children(2)->innertext, 'TURBIDITY') !== false) {
			$counts[7]++;
			$vals[7]+=$e->children(6)->innertext;
		}
		if (strpos($e->children(2)->innertext, 'TOTAL DISSOLVED SOLIDS') !== false) {
			$counts[8]++;
			$vals[8]+=$e->children(6)->innertext;
		}
	}
	/*	print_r($counts);
	echo"<br>";
	print_r($vals);
	echo"<br>";*/
}


/*$detailedReport = "https://sdwis.waterboards.ca.gov/PDWW/JSP/SamplingResultsByStoret.jsp?SystemNumber=".$pwsid."&SamplingPointName=&SamplingPointID=001&Storet=&ChemicalName=";
$detailedReport = $detailedReport."&begin_date=".$month."%2F".$day."%2F".($year - 1);
$detailedReport = $detailedReport."&end_date=".$month."%2F".$day."%2F".$year;
$detailedReport = $detailedReport."&Generate+Report=Generate+Report";

echo $detailedReport;*/

//$html = file_get_html($detailedReport);



/*parse through the table looking for values that we know exist, and pull those values for use in quality calculation*/
/*foreach($html->find('#3 tbody tr') as $e){
	//if(check for disollved oxygen){$vals[0]+=result;}
	//if(check for fecal coliform){$vals[1]+=result;}
	if (strpos($e->children(2)->innertext, 'PH,') !== false) {
		$counts[2]++;
		$vals[2]+=$e->children(6)->innertext;
	}
	//if(check for BOD){vals[3]+=result;}
	//if(check for TemperatureChange){vals[4]+=result;}
	if (strpos($e->children(2)->innertext, 'PHOSPHATE,') !== false) {
		$counts[5]++;
		$vals[5]+=$e->children(6)->innertext;
	}
	if (strpos($e->children(2)->innertext, 'NITRATE') !== false) {
		//4.42 converts from nitrate in form N to nitrate in form no3
		$counts[6]++;
		$vals[6]+=($e->children(6)->innertext*4.42);
	}
	if (strpos($e->children(2)->innertext, 'TURBIDITY') !== false) {
		$counts[7]++;
		$vals[7]+=$e->children(6)->innertext;
	}
	if (strpos($e->children(2)->innertext, 'TOTAL DISSOLVED SOLIDS') !== false) {
		$counts[8]++;
		$vals[8]+=$e->children(6)->innertext;
	}
}*/
/*
To handle missing values we can
	-reweight, add the extra weight to the rest of the values evenly
	-use weights, and use the average value of each as a substitute for the missing value

*Note that reweigting is used by http://www.water-research.net/

These are the weights taht we use for each of our parameters, we are using the adjusted weight method, so if a value is missing
we increase all the other weights proportionately to allow for a score to still be given.
*/


/*Get the average from the sum of all the results found over the last year*/
for ($index = 0; $index < 9; $index++) {
	if($counts[$index]!=0)
	{
		$vals[$index] = $vals[$index]/$counts[$index];
	}else{$adjustedWeight+=$weights[$index];}
}
/*adjust the weights to account for missing values*/
for ($index = 0; $index < 9; $index++) {
	if((1-$adjustedWeight) != 0){
		$weights[$index] *= (1/(1-$adjustedWeight));
	}
}




/*using the average of all the results over the last year we get the results from our functions, multiply them by their respective weights,
and we sum the values to get a score for this particular water source*/
for ($index = 0; $index < 9; $index++) {
	$result = 0;
	if($counts[$index]!=0)
	{
		$result = $weights[$index] * call_user_func($functions[$functionNames[$index]], $vals[$index]);
		//	echo "result is: ".$result."<br>";
		$vals[$index] = $vals[$index]/$counts[$index];
	}
	$score += $result;
}
foreach($counts as $count)
{
	echo $count.",";
}
echo $score;


//$valForDisolvedOxygen=
/*Disolved Oxygen function defined for values 0-140 if >140 val = 50*/

//$valForFecalColiform = (0.16)*(1.114311 + (100.3168 - 1.114311)/(1 + ($Val/60.12186)^0.47579));
/*Fecal Coliform function defined for values 1-100000 if >100000 val = 2*/

//$valForPH = (0.11)*(90.16164*M_E^(-($Val - 7.412419)^2/4.33285558128));
/*PH function defined for values 2-12 if >12 val = 0*/

//$valForBOD = (0.11)*(99.49688 - (11.26095/0.1157408)*(1 - M_E^(-0.1157408*$Val)));
/*Biochemical oxygen Demand function defined for values 0-30 if >30 val = 2*/

//$valForTempChange = (0.10)*(82.48728*M_E^(-($Val - -0.4976515)^2/(2*11.12208^2)) );
/*Temp change function defined for values -10-30 (celcius)*/

//$valForPhosphate = (0.10)*(98.18296 - (76.77346/0.8585003)*(1 - M_E^(-0.8585003*$Val)));
/*phosphate function defined for values 0-10, if >10 val = 2*/

//$valForNitrates =  (0.10)*(96.5569 - (4.809955/0.05150035)*(1 - M_E^(-0.05150035*$Val)));
/*nitrates function defined for values 0-100, if >100 val = 1*/

//$valForTurbidity = (0.8)*(98.25774 - (2.17882/0.02553104)*(1 - M_E^(-0.02553104*$Val)));
/*turbidity function defined for values 0-100, if >100 val = 5*/

//$valForTotalSolids = (0.7)*(80.13986 + 0.2043823*$Val - 0.002302098*$Val^2 + 0.000006573427*$Val^3 - 6.526807e-9*$Val^4);
/*total solids function defined for values 0-500 if > 500 val = 20*/


/*echo "<br>"."https://sdwis.waterboards.ca.gov/PDWW/JSP/SamplingResultsByStoret.jsp?SystemNumber=0110010&SamplingPointName=DEL+VALLE+WTP&SamplingPointID=001&Storet=&ChemicalName=&begin_date=01%2F02%2F2017&end_date=01%2F02%2F2017&Generate+Report=Generate+Report";*/

/*$html = file_get_html($url);*/

// Find all "A" tags and print their HREFs
/*$count = 0;
foreach($html->find('#AutoNumber7 tbody tr') as $e){
	//echo "status".$e->children ( 3 )->innertext."type".$e->children ( 2 )->innertext;
	if($e->children ( 3 )->innertext == "A")//ensure that it is an active water source, and that it serves residents
	{

		if(str_replace(' ', '', $e->children ( 2 )->innertext) =="C")
		{
			$count = $count+1;
		}

	}

}
echo $count;*/
?>
