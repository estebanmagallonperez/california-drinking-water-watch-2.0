var bandwidth = 0;
var map = L.map('map').setView([37, -119.5], 6);
function highlightFeature(e) {
	var layer = e.target;

	layer.setStyle({
		weight: 5
	});
}
function zoomToFeature(e) {
	map.fitBounds(e.target.getBounds());
	info.update(e);

}
var colors
var geojson;
function resetHighlight(e) {
	e.target.setStyle({
		weight: 1
	});
}
var asdf ="";
$.getJSON("./map/asdf.geojson", function(json) {
	function onEachFeature(feature, layer)
	{
		layer.on({
			mouseover: highlightFeature,
			mouseout: resetHighlight,
			click: zoomToFeature
		});
	}


	geojson = L.geoJSON(json, {
		style: style,
		onEachFeature: onEachFeature
	}).addTo(map);
	asdf = geojson;
});
function makeString()
{
	$("body").html(JSON.stringify(asdf, null, 4));
	console.log(JSON.stringify(asdf, null, 4));
}

var info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.set();
    return this._div;
};
info.set = function (e) {
    this._div.innerHTML = '<h4>Selected Water Districts</h4>' + 'Click on a Water District';
};

var toRevert = [];
info.update = function (e) {
	console.log(toRevert);
	for(var i = 0;i <toRevert.length;i++)
	{
		toRevert[i].setStyle({
		weight: 1,
		color: '#717073',
		dashArray: '',
		fillOpacity: 0.7,
		fillColor:'#919195'
	});
	}
	var lat = e.latlng.lat;
	var long = e.latlng.lng;
	toRevert = leafletPip.pointInLayer([long, lat],asdf,false);
	var output = toRevert.length>0?"":'Click on a Water District';
	console.log(toRevert);
	for(var i = 0;i < toRevert.length; i ++)
	{
		toRevert[i].setStyle({
		weight: 5,
		color: '#717073',
		dashArray: '',
		fillOpacity: 0.7,
		fillColor:'#80A1B6'
	});
		output += "<a href='#'>"+toRevert[i].feature.properties.pwsname+"</a><br>";
	}
    this._div.innerHTML = '<h4>Your Water Districts</h4>' +  output;
};

info.addTo(map);




function setPopup(feature,layer)
{
	var result = feature.properties.county;
	var urlName = feature.properties.county;
	layer.bindPopup("You are in "+feature.properties.county+" County, likely served by the "+feature.properties.pwsname+"<br><br><a style='float:right;' href='test.php?"+urlName+"'>Lets Find Yours</a><br>");
	layer.setStyle({
		fillColor: 'red'
	});

}

function calcArea(feature)
{

	var area = 0;
	for(var i = 0; i <feature.geometry.coordinates.length;i++)
	{
		var left = 0;
		var right = 0;
		var array = feature.geometry.coordinates[i][0];
		array.push( array[0]);
		var temp = array.length;
		for(var j =0; j <temp -1;j++)
		{

			left += array[j][0]*array[j+1][1];
			right += array[j][1]*array[j+1][0];
		}
		area += right - left;
	}
	feature.properties.area = area;

}
function style(feature)
{
	return ({
		weight: 1,
		color: '#717073',
		dashArray: '',
		fillOpacity: 0.7,
		fillColor:'#919195'
	});
}

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
	maxZoom: 18,
	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
	'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
	id: 'mapbox.light'
}).addTo(map);
