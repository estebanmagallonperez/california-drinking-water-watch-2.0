var counties = ["San%20Francisco","Santa%20Cruz","Amador","Sutter","Yuba","San%20Mateo","Alpine","Napa","Contra%20Costa","Alameda","Marin","Orange","Solano","Sierra","Nevada","Sacramento","Yolo","Calaveras","Colusa","Santa%20Clara","Del%20Norte","Lake","Kings","Glenn","San%20Benito","San%20Joaquin","Mariposa","Stanislaus","Placer","Butte","Sonoma","El%20Dorad","Merced","Ventura","Madera","Tuolumne","Plumas","Tehama","Mono","Trinity","San%20Luis%20Obispo","Santa%20Barbara","Monterey","Mendocino","Shasta","Humboldt","Imperial","San%20Diego","Modoc","Los%20Angeles","Tulare","Lassen","Fresno","Siskiyou","Riverside","Kern","Inyo","San%20Bernardino"];
console.log(counties);
getCounty(0);
var myvals = [0,0,0,0];
function getCounty(currentIndex)
{
	var urlName = counties[currentIndex];
	var jqxhr = $.ajax( {url: "./php/loadCounty.php?county="+urlName,
						 timeout:10000 })
	.done(function() {
		var out = jqxhr.responseText;
		if(out.indexOf("error") >=0)
		{
			getCounty(currentIndex);
		}else
		{

			var result = jqxhr.responseText;
			//console.log(currentIndex+"("+urlName+"): "+result);
			var vals = result.split(",");
			addChart(urlName,currentIndex,vals);
			//console.log(vals);
			(currentIndex < counties.length -1)?getCounty(currentIndex+1):imlazy(currentIndex);

		}
	})
	.fail(function() {
		getCounty(currentIndex);
	})
	}
function imlazy(currentIndex)
{
	/*	console.log(myvals);
	for(var i =0;i<myvals.length;i++)
	{
		myvals[i] = myvals[i]/currentIndex;
	}
	console.log(myvals);*/
	addChart("average",currentIndex+1,myvals)
}
function addChart(name,count,values) {
	$("body").append('<div id="piechart'+count+'" style="height: 300px; width: 100%;"></div>')
	var sum = 0;
	for(var i =0;i<values.length;i++)
	{
		values[i] = parseInt(values[i]);
		/*sum+=values[i];*/
	}
	//console.log(values);
	for(var i =0;i<values.length;i++)
	{
		if(values != myvals){
			myvals[i]+=values[i];
		}/*
		values[i] = (values[i]/sum).toFixed(2) *100;*/
	}
	//console.log(myvals);
	google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Type', 'Count'],
          ['Community',     values[0]],
          ['Transient Non-Community',      values[1]],
          ['Non-Transient Non-Community',  values[2]],
          ['NP(not listed in glossary)', values[3]]
        ]);

        var options = {
          title: name.split("%20").join(" ")+" Distribution(C,NC,NTNC,NP)"
        };
		var toFind = 'piechart'+count;
		//  console.log(toFind);
		//console.log(document.getElementById(toFind));
        var chart = new google.visualization.PieChart(document.getElementById('piechart'+count));

        chart.draw(data, options);
      }
	/*var chart = new CanvasJS.Chart("chartContainer"+count,
								   {
		title:{
			text: name.split("%20").join(" ")+" Distribution(C,NC,NTNC,NP)"
		},
		animationEnabled: false,
		legend:{
			verticalAlign: "center",
			horizontalAlign: "left",
			fontSize: 20,
			fontFamily: "Helvetica"
		},
		theme: "theme2",
		data: [
			{
				type: "pie",
				indexLabelFontFamily: "Garamond",
				indexLabelFontSize: 20,
				indexLabel: "{label} {y}%",
				startAngle:-20,
				showInLegend: true,
				toolTipContent:"{label} {y}%",
				dataPoints: [
					{  y: values[0], legendText:"C", label: "Community" },
					{  y: values[1], legendText:"NC", label: "Transient Non-Community" },
					{  y: values[2], legendText:"NTNC", label: "Non-Transient Non-Community" },
					{  y: values[3], legendText:"NP" , label: "NP(not listed in glossary)"}
				]
			}
		]
	});
	chart.render();*/
}
