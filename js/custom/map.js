var geojson;
var groupedLayer = [];
var searchJson;
function mapSetup(){

	var bandwidth = 0;
	var map = L.map('map').setView([37, -119.5], 6);
	map.scrollWheelZoom.disable();
	function highlightFeature(e) {
		var layer = e.target;

		layer.setStyle({
			weight: 5
		});
	}
	function zoomToFeature(e) {
		/*	console.log("bounds");
	console.log(e.target.getBounds());
	var temp = e.target.getBounds();
	console.log("bounds");
	console.log(e.target.getBounds());
	map.fitBounds(temp);*/
		info.update(e);

	}
	var colors
	function resetHighlight(e) {
		e.target.setStyle({
			weight: 1
		});
	}
	for(let i =0;i<25;i++)
	{
		setTimeout(function(){addToMap(i)},1);
	}
	//var joined =-1;

	$.ajax({
		dataType: "json",
		cache:"true",
		url: './map/search.geojson',
		success: function(json){

		}
	}).done(function(json){
		searchJson = json;
	});





	function addToMap(piece){
		$.ajax({
			dataType: "json",
			cache:"true",
			url: './map/pieces/piece'+piece+'.geojson',
			success: function(json){

			}
		}).done(function(json){
			function onEachFeature(feature, layer)
			{
				layer.on({
					mouseover: highlightFeature,
					mouseout: resetHighlight,
					click: zoomToFeature
				});
			}
			groupedLayer.push(
				new L.geoJSON(json, {
					style: style,
					onEachFeature: onEachFeature
				})
			);
			if( groupedLayer.length ==25)
			{
				geojson = L.layerGroup(groupedLayer).addTo(map);
			}

		});
	}
	var info = L.control();
	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
		this.set();
		return this._div;
	};
	info.set = function (e) {
		this._div.innerHTML = '<h4>Selected Water Districts</h4>'+'<div style="text-align:center;"><label for="city">Search by City</label><input id="city" /><span></span><label for="county">Search by County</label><input id="county" /><span></span></div>' + '<div style="text-align:center;"><br>-OR-<br><br>Click on a Water District</div>';
		setTimeout(function(){
			$('#city').bind('input', function() {
				var searchString = "<br>";
				var count = 0;
				if($(this).val() =="")
				{
					$(this).next().html("");
					return;
				}
				for(var i = 0; i < searchJson.length-1 && count < 5;i++)
				{
					var city = searchJson[i].properties.loccity;
					var search = $(this).val();
					if(city!=null){
						if( city.toLowerCase().indexOf(search.toLowerCase()) > -1)
						{
							searchString+= "<a href='./display.php?tinwsys="+ searchJson[i].properties.tinSys+"&pwsid="+searchJson[i].properties.pwsid+"&name="+searchJson[i].properties.pwsname+"'>"+searchJson[i].properties.pwsname+"</a>"+ "<br>";
							count++;
						}
					}
				}
				$(this).next().html(searchString);
			});
			$('#county').bind('input', function() {
				var searchString = "<br>";
				var count = 0;
				if($(this).val() =="")
				{
					$(this).next().html("");
					return;
				}
				for(var i = 0; i < searchJson.length-1 && count < 5;i++)
				{
					var county = searchJson[i].properties.county;
					var search = $(this).val();
					if(county!=null){
						if(county.toLowerCase().indexOf(search.toLowerCase()) > -1)
						{
							searchString+= "<a href='./display.php?tinwsys="+ searchJson[i].properties.tinSys+"&pwsid="+searchJson[i].properties.pwsid+"&name="+searchJson[i].properties.pwsname+"'>"+searchJson[i].properties.pwsname+"</a>"+ "<br>";
							count++;
						}
					}
				}
				$(this).next().html(searchString);
			});
		},10);
	};
	var toRevert = [];
	info.update = function (e) {
		for(var i = 0;i <toRevert.length;i++)
		{
			toRevert[i].setStyle({
				weight: 1,
				color: '#717073',
				dashArray: '',
				fillOpacity: 0.7,
				fillColor:'#919195'
			});
		}
		var lat = e.latlng.lat;
		var long = e.latlng.lng;
		toRevert = [];
		for(let propName in geojson._layers)
		{
			var output = leafletPip.pointInLayer([long, lat],geojson._layers[propName],false);
			for(var i = 0; i <output.length;i++)
			{

				toRevert.push(output[i]);
			}
		}
		//toRevert = leafletPip.pointInLayer([long, lat],geojson,false);
		var output = toRevert.length>0?"":'Click on a Water District';
		for(var i = 0;i < toRevert.length; i ++)
		{
			toRevert[i].setStyle({
				weight: 5,
				color: '#717073',
				dashArray: '',
				fillOpacity: 0.7,
				fillColor:'#80A1B6'
			});
			output += "<a href='./display.php?tinwsys=" +toRevert[i].feature.properties.tinSys+"&pwsid="+toRevert[i].feature.properties.pwsid+"&name="+toRevert[i].feature.properties.pwsname+"'>"+toRevert[i].feature.properties.pwsname+"</a><br>";
		}
		this._div.innerHTML = '<h4>Your Water Districts</h4>'+'<div style="text-align:center;"><label for="city">Search by City</label><input id="city" /><span></span><label for="county">Search by County</label><input id="county" /><span></span></div>' + '<div style="text-align:center;"><br>-OR-<br><br>Click on a Water District</div>' +  output ;
		setTimeout(function(){
			$('#city').bind('input', function() {
				var searchString = "<br>";
				var count = 0;
				if($(this).val() =="")
				{
					$(this).next().html("");
					return;
				}
				for(var i = 0; i < searchJson.length-1 && count < 5;i++)
				{
					var city = searchJson[i].properties.loccity;
					var search = $(this).val();
					if(city!=null){
						if( city.toLowerCase().indexOf(search.toLowerCase()) > -1)
						{
							searchString+= "<a href='./display.php?tinwsys="+ searchJson[i].properties.tinSys+"&pwsid="+searchJson[i].properties.pwsid+"&name="+searchJson[i].properties.pwsname+"'>"+searchJson[i].properties.pwsname+"</a>"+ "<br>";
							count++;
						}
					}
				}
				$(this).next().html(searchString);
			});
			$('#county').bind('input', function() {
				var searchString = "<br>";
				var count = 0;
				if($(this).val() =="")
				{
					$(this).next().html("");
					return;
				}
				for(var i = 0; i < searchJson.length-1 && count < 5;i++)
				{
					var county = searchJson[i].properties.county;
					var search = $(this).val();
					if(county!=null){
						if(county.toLowerCase().indexOf(search.toLowerCase()) > -1)
						{
							searchString+= "<a href='./display.php?tinwsys="+ searchJson[i].properties.tinSys+"&pwsid="+searchJson[i].properties.pwsid+"&name="+searchJson[i].properties.pwsname+"'>"+searchJson[i].properties.pwsname+"</a>"+ "<br>";
							count++;
						}
					}
				}
				$(this).next().html(searchString);
			});
		},10);
	};

	info.addTo(map);



	//We no longer use a popup
	/*	function setPopup(feature,layer)
	{
		var result = feature.properties.county;
		var urlName = feature.properties.county;
		layer.bindPopup("You are in "+feature.properties.county+" County, likely served by the "+feature.properties.pwsname+"<br><br><a style='float:right;' href='test.php?"+urlName+"'>Lets Find Yours</a><br>");
		layer.setStyle({
			fillColor: 'red'
		});

	}*/
	//Used for sorting array, no longer needed
	/*function calcArea(feature)
	{

		var area = 0;
		for(var i = 0; i <feature.geometry.coordinates.length;i++)
		{
			var left = 0;
			var right = 0;
			var array = feature.geometry.coordinates[i][0];
			array.push( array[0]);
			var temp = array.length;
			for(var j =0; j <temp -1;j++)
			{

				left += array[j][0]*array[j+1][1];
				right += array[j][1]*array[j+1][0];
			}
			area += right - left;
		}
		feature.properties.area = area;

	}*/
	function style(feature)
	{
		return ({
			weight: 1,
			color: '#717073',
			dashArray: '',
			fillOpacity: 0.7,
			fillColor:'#919195'
		});
	}

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
		'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
		id: 'mapbox.light'
	}).addTo(map);
}

